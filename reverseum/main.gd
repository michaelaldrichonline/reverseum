extends Node2D

var BallScene = preload("res://reverseum/scenes/ball.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var ball = BallScene.instance()
	ball.position = Vector2(0, 0)
	add_child(ball)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
